# statocles

SSG using Statocles

## Initial site creation command

```docker run --rm -v $PWD:/usr/src/app:rw -it registry.gitlab.com/madmongers/statocles:latest statocles create```

Choose deploy option to copy files: public

Note: using docker to run statocles will cause the files to be owned by root, so you will want to chown them

## New blog post

```
docker run --rm -v $PWD:/usr/src/app:rw -it registry.gitlab.com/madmongers/statocles:latest statocles blog post Welcome
chown -R $USER blog
```
