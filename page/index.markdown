---
title: Statocles
---

# Statocles SSG

Statocles is a Static Site Generator (SSG) written in Perl.  

[Check the documentation for how to write Statocles
content](http://preaction.me/statocles/pod/Statocles/Help/Content)