docker run --rm \
  -v $PWD/public:/usr/src/app:ro \
  -p 127.0.0.1:8080:8080 \
  -t registry.gitlab.com/madmongers/statocles:latest \
  perl -MMojolicious::Lite -MCwd -e 'app->static->paths->[0]=getcwd; app->start' daemon -l http://*:8080